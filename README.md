# springcloud-idea-chapter02-s

#### 介绍
第二章Eurekaserver代码

#### 软件架构
Spring Cloud 

Eureka Server 

#### 配置确认 IDEA环境

1. 安装 Git，并配置

![aa](./doc/git配置.png)

2. 配置 Maven加速，配置好 settings.xml 在 .m2目录下

![aa](./doc/maven.png)

3. 克隆项目  
  新建项目  
   ![aa](./doc/newp.png) 

4. 导入依赖

![aa](./doc/maven2.png)

5. 运行，客户端访问

![aa](./doc/效果.png)


![aa](./doc/eureka.png)

![aa](./doc/image.png)